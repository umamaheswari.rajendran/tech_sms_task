Rails.application.routes.draw do


  get 'logout' => 'sessions#logout'
  get 'login' => 'sessions#login'
  post 'login_process' => 'sessions#login_process'
  get 'welcome_user' => 'sessions#welcome_user'
  get 'student_rank' => 'students#student_rank'

  
  resources :student_marks
  resources :subjects
  resources :students
  resources :users
  resources :roles

  root 'sessions#login'

  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
