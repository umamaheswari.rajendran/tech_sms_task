class CreateUsers < ActiveRecord::Migration[6.0]
  def change
    create_table :users do |t|
      t.string :user_name
      t.string :email_id
      t.string :password_digest
      t.string :education_qualification
      t.references :role, null: false, foreign_key: true

      t.timestamps
    end
  end
end
