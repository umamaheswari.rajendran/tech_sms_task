class CreateStudentMarks < ActiveRecord::Migration[6.0]
  def change
    create_table :student_marks do |t|
      t.references :student, null: false, foreign_key: true
      t.references :subject, null: false, foreign_key: true
      t.references :user, null: false, foreign_key: true
      t.integer :mark

      t.timestamps
    end
  end
end
