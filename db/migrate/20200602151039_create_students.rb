class CreateStudents < ActiveRecord::Migration[6.0]
  def change
    create_table :students do |t|
      t.string :stud_name
      t.datetime :stud_dob
      t.string :age
      t.string :total_mark, default: 0
      t.integer :rank

      t.timestamps
    end
  end
end
