class Subject < ApplicationRecord
  belongs_to :user

  validates_uniqueness_of :user_id #, scope: :subject_id
  validates_uniqueness_of :sub_code, presence: true, :message => "Already Exists"
end
