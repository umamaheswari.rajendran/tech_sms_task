class StudentMark < ApplicationRecord
  belongs_to :student
  belongs_to :subject
  belongs_to :user

  # validates :student_id, uniqueness: true
  # validates :subject_id, uniqueness: true
  # validates :user_id, uniqueness: true

  validates_uniqueness_of :subject_id, scope: %i[user_id student_id]

end
