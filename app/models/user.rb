class User < ApplicationRecord
  belongs_to :role
  
  require 'bcrypt'

  has_secure_password

    
  validates_uniqueness_of :user_name, presence: true, :message => "Already Exists"
  validates_uniqueness_of :email_id, :message => "Already Exists"

end
