json.extract! student_mark, :id, :student_id, :subject_id, :user_id, :mark, :created_at, :updated_at
json.url student_mark_url(student_mark, format: :json)
