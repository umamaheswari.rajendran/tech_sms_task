json.extract! subject, :id, :sub_name, :sub_code, :user_id, :created_at, :updated_at
json.url subject_url(subject, format: :json)
