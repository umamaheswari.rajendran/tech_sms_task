json.extract! student, :id, :stud_name, :stud_std, :stud_dob, :age, :created_at, :updated_at
json.url student_url(student, format: :json)
