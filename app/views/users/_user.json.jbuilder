json.extract! user, :id, :user_name, :email, :password, :education_qualification, :role_id, :created_at, :updated_at
json.url user_url(user, format: :json)
