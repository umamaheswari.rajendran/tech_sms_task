require 'active_support/concern'
module RankCalculation
	extend ActiveSupport::Concern

	def self.rank_calculate(student_id)
		# total_mark_update = StudentMark.all.group_by(&:student_id).map{|id, mark| Student.find(id).update!(total_mark: mark.pluck(:mark).sum)}
		total_mark = StudentMark.where(student_id: student_id).pluck(:mark).sum
		total_update = Student.find(student_id).update(total_mark: total_mark)
		students = Student.all
		student_mark = students.group_by(&:total_mark)
		rankwise_data = {}
		student_mark.each{|k, v| student_mark[k] = v.count}
		mark_order = Hash[student_mark.sort_by{|k, v| k.to_i}.reverse]

		mark_order.each do |tot_mark, stud_count|
			pre_mark = rankwise_data.values.last if rankwise_data != {}
			if pre_mark == nil 
				num = 1
			else
				num = mark_order.fetch(pre_mark) + rankwise_data.select{|k, v| v == pre_mark}.keys.to_sentence.to_i # For 1.Fetching a count by using previous mark & 2.Fetching a previous rank(keys) by using previous hash value
			end
			rankwise_data.merge!({num => tot_mark})
		end
		ranked_marks = rankwise_data.values.flatten if rankwise_data.present? && rankwise_data != {}
		students.map do |student|
			if rankwise_data.present? && rankwise_data != {}
				rankwise_data.each{|rank, total_mark| total_mark.include?(student.total_mark) ? student.update!(rank: rank) : ranked_marks.include?(student.total_mark) ? next : student.update!(rank: 0) }
			else
				student.update!(rank: 0) if student.rank != 0
			end
		end
	end

end