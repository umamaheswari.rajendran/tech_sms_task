class ApplicationController < ActionController::Base
	before_action :check_session
	helper_method :current_user

	# for accessing the methods written in the concern named exception_handler.rb
    include RankCalculation

	def check_session
		redirect_to login_path unless current_user
	end

	private
	
	def current_user
		@current_user ||= User.find(session[:user_id]) if session[:user_id]
	end
end
